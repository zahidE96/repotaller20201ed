var clientesObtenidos;
function getClientes() {
  //var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange=function (){
    if (this.readyState==4 && this.status==200) {
      // una petición tiene 4 estados cxódigos de respuesta
      //console.table(JSON.parse(request.responseText).value); //va a recibir el JSON en una tabla
      clientesObtenidos = request.responseText;
      procesarClientes();

    }
  }
  request.open("GET", url, true);//TIPO GET
  request.send();

  function procesarClientes() {
    var JSONProductos=JSON.parse(clientesObtenidos);
    //alert(JSONProductos.value[0].ProductName);
    var divTabla = document.getElementById("divTablaClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");
    //+++ Bandera
    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    //bandera
    for(var i=0; i<JSONProductos.value.length; i++){
      //console.log(JSONProductos.value[i].ProductName);
      //--------para clientes contactname city country
      var nuevaFila=document.createElement("tr");
      var columnaNombre = document.createElement("td");
      columnaNombre.innerText=JSONProductos.value[i].ContactName;
      var columnaCiudad = document.createElement("td");
      columnaCiudad.innerText=JSONProductos.value[i].City;
      var columnaPais = document.createElement("td");
      //columnaPais.innerText=JSONProductos.value[i].Country;

      var imgPais = document.createElement("img");
      if (JSONProductos.value[i].Country == "UK") {
     imgPais.src = rutaBandera + "United-Kingdom.png";
   }else {
     imgPais.src = rutaBandera+JSONProductos.value[i].Country+".png";
   }
   imgPais.style.width = "70px";
   imgPais.style.height = "40px";
   columnaPais.appendChild(imgPais);
   //columnaPais.innerText = JSONClientes.value[i].Country;
      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaCiudad);
      nuevaFila.appendChild(columnaPais);
      tbody.appendChild(nuevaFila);

    }
    tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
  }

}
